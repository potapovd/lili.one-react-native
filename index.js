/** @format */

import React from "react";

import { AppRegistry } from "react-native";
import lilione from "./App";
import 'react-native-gesture-handler'

AppRegistry.registerComponent('Lilione', () => lilione)
