import AppConfig from './AppConfig.json'

export default {
    // black and white theme
    main: '#FFFFFF', // '#1CB5B4',
    toolbar: 'rgba(255, 255, 255, 0.98)',
    toolbarTint: AppConfig.MainColor,
    toolbarStickyHeader: "rgba(42, 181, 179, 0.95)",
    text: '#333333',
    menuCategory: "rgba(255, 255, 255, .98)",
    menuCategoryActive: "transparent",
    menuCategoryActiveText: "rgba(42, 181, 179, 1)",
    menuCategoryActiveBorder: "rgba(42, 181, 179, 1)",
    menuItem: "#333333",

    tabbar: '#FADF55',
    tabbarTint: '#000',
    tabbarColor: "#777",
    tabActiveText: '#fff',
	//FIXES
    headerViewBottomBorder:'#c0c0c2',
	tabbarTopBorder:'#c0c0c2',
	containerBackground:'#FADF55',
    headerDetailbackgroundColor:'#FADF55',
    headerBackButton:AppConfig.MainColor,
    headerIndicator:'#373624',
    categoriesColor:'#373624',
    playButton: 'rgba(255,212,1, 0.55)',
    playButtonArrow: '#373624',
    swiperColor: '#ffd401',

    // // color theme
    // main: '#1CB5B4', //'#7f8c8d', // '#c0392b', // '#8e44ad', // '#34495e', //'#1CB5B4',
    // toolbarTint: "#1CB5B4",
    // toolbar: '#FFFFFF',
    // text: '#FFFFFF',
    //
    // menuCategory: "rgba(0, 0, 0, 0.4)",
    // menuCategoryActive: "rgba(255, 255, 255, 0.1)",
    // menuCategoryActiveText: "rgba(255, 255, 255, 1)",
    // menuCategoryActiveBorder: "rgba(255, 255, 255, 0.1)",
    // menuItem: "#ccc",

    spin: '#A58717',
    time: '#aaaaaa',
    title: '#333333',
    colors: [
        '#43C59E',
        '#14453D',
        '#7A4419',
        '#515A47',
        '#3CDBD3',
        '#7DCE82',
        '#8A84E2',
        '#84AFE6',
        '#AFAFDC',
        '#DB5A42',
        '#AC7B84',
        '#373F51',
        '#DAA49A'
    ]
}
