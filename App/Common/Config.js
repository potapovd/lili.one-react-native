/** @format */

import Constants from './Constants';

export default {
  /**
   * The detail document from: https://beonews.inspireui.com
   * Step 1: Moved to AppConfig.json
   */
  // IsRequiredLogin:true,
  Banner: {
    visible: false,
    sticky: true,
    tag: [],
    categories: [],
  },

  /**
   * Advance config
   * CategoryVideo: config the category id for video page
   *
   * */
  // Category video id from the menu
  CategoryVideo: 53,

  imageCategories: {
    //  bioenergetika: require('@images/category/cat0.png'),
    // blog: require('@images/category/cat4.png'),
    // meditatsiya_dnia:require('@images/category/cat2.png'),
    // meditatsiya_dvegenie:require('@images/category/cat8.png'),
    // meditatsiya:require('@images/category/cat1.png'),
    // meditatsiya_osoznannosti:require('@images/category/cat5.png'),
    // staticheskaya_meditatsiya:require('@images/category/cat6.png'),
    // pozitivnaya_psihologiya:require('@images/category/cat9.png'),
    // son:require('@images/category/cat3.png'),
    // tsigun:require('@images/category/cat7.png'),
  },

  // Custom page from left menu side
  CustomPages: {
    contact_id: 11,
    aboutus_id: 8513,
  },

  // config for Firebase, use to sync user data across device and favorite post
  Firebase: {
    // apiKey: 'AIzaSyDnBpxFOfeG6P06nK97hMg01kEgX48JhLE',
    // authDomain: 'beonews-ef22f.firebaseapp.com',
    // databaseURL: 'https://beonews-ef22f.firebaseio.com',
    // storageBucket: 'beonews-ef22f.appspot.com',
    // messagingSenderId: '1008301626030',
    // readlaterTable: 'list_readlater',
  },

  // config for log in by Facebook
  Facebook: {
    showAds: false,
    adPlacementID: '',
    logInID: '',
    sizeAds: 'standard', // standard, large
  },

  // config for log in by Google
  // Google: {
  //   analyticId: 'UA-90561349-1',
  //   androidClientId:
  //     '338838704385-1om86241pq2qpg4qi677jb1ndo5jqfh2.apps.googleusercontent.com',
  //   iosClientId:
  //     '338838704385-1om86241pq2qpg4qi677jb1ndo5jqfh2.apps.googleusercontent.com',
  // },

  // The advance layout
  AdvanceLayout: [
    Constants.Layout.threeColumn,
    Constants.Layout.threeColumn,
    Constants.Layout.threeColumn,
    Constants.Layout.list,
    Constants.Layout.list,
    Constants.Layout.list,
    Constants.Layout.list,
    Constants.Layout.card,
    Constants.Layout.column,
    Constants.Layout.column,
  ],

  // config for log in by Admob
  AdMob: {
    visible: false,
    deviceID: '',
    unitID: '',
    unitInterstitial: '',
    isShowInterstital: false,
  },

  // tab animate
  tabBarAnimate: Constants.Animate.zoomIn,

  // config default for left menu
  LeftMenuStyle: Constants.LeftMenu.scale,

  notification: {
    // AppId: '85cbc2b5-4e0d-4214-9653-8054d06f4256',
    // NewAppId: '88b0e176-5756-47b7-b061-aacea262421d',
  },

  OneSignal: {
     appId: '6eb72c24-9d0d-42f2-afb6-4d8620aba805',
  },

  // update 18 May
  // showLayoutButton: Ability to show/hide the switch layout button at homescreen
  // homeLayout: default homeLayout UI: Constants.Layout.mansory, Constants.Layout.horizontal
  // showSwitchCategory: show the switch button on categories page
  showLayoutButton: false,
  homeLayout: Constants.Layout.horizontal,
  showSwitchCategory: false,
  showAppIntro: false,
  EnableSanitizeHtml: true,
  RequiredLogin: false,
  showSubCategoryScreen: false,

  intro: [
    // {
    //   title: 'Welcome to BeoFoods',
    //   description:
    //     'Get the latest food receipt on your hand and sharing with your friends',
    //   backgroundColor: '#D5E8ED',
    //   source: require('@images/lottie/loading.json'),
    //   button: 'GET STARTED',
    // },
    // {
    //   title: 'Easy to use',
    //   description:
    //     'The app is well design and smoothly navigation between screens',
    //   backgroundColor: '#FFF',
    //   source: require('@images/lottie/check_animation.json'),
    //   button: 'NEXT',
    // },
    // {
    //   title: 'Push notification',
    //   description:
    //     'Get up-to-date the content regularly and offline mode is supported',
    //   backgroundColor: '#D6D7DD',
    //   source: require('@images/lottie/notification.json'),
    //   button: 'DONE',
    // },
  ],
};
