/** @format */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Languages} from '@common'
import {Bot} from '@container'

class BotScreen extends Component {
    static navigationOptions = {
        tabBarLabel: Languages.textBookMark,
        headerShown: false,
    }

    static propTypes = {
        navigation: PropTypes.object,
    }

    render = () => {
        const {navigate} = this.props.navigation
        return (
                <Bot/>
            // <Search
            //     onViewPost={(post, index) =>
            //         navigate('searchPostDetail', { post, index, fromSearch: true })
            //     }
            // />
        )
    }
}

export default BotScreen
