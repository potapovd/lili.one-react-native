/** @format */

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {ScrollView, Image, View, Text} from 'react-native';
import Animated from 'react-native-reanimated';

import {flatten} from 'lodash';
import {
    fetchCategories,
    setActiveLayout,
    setActiveCategory,
} from '@redux/actions';
import {connect} from 'react-redux';
import {Color, Languages, Config, Images} from '@common';
import {CategoryList, TouchableScale, AnimatedHeader, Analytics} from '@components';
import FAB from '@custom/react-native-fab';
import Icon from '@expo/vector-icons/FontAwesome';
import styles from './styles';

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

class CategoryHome extends PureComponent {
    static propTypes = {
        categories: PropTypes.array,
        fetchCategories: PropTypes.func,
        setActiveCategory: PropTypes.func,
        onViewPost: PropTypes.func,
        onViewCategory: PropTypes.func,
        setActiveLayout: PropTypes.func,
        selectedLayout: PropTypes.bool,
    };

    state = {
        scrollY: new Animated.Value(0),
    };

    componentDidMount() {
        this.props.fetchCategories();
    }

    fireAnalytics = async (category)=> {
        await Analytics.onCategoryClick(category)
    }

    showCategory = (category) => {
        this.fireAnalytics(category);
        const {setActiveCategory, onViewCategory} = this.props;
        setActiveCategory(category.id);
        onViewCategory({config: {name: category.name, category: category.id}});
    };

    changeLayout = () => {
        this.props.setActiveLayout(!this.props.selectedLayout);
    };

    renderContent = () => {
        const {categories, onViewPost, selectedLayout} = this.props;

        if (!selectedLayout) {
            return <CategoryList showBanner onViewPost={onViewPost}/>;
        }
        const backgroundColors = [
            "rgba(255, 212, 0, .33)","rgba(138, 88, 138, .33)", "rgba(236, 126, 173, .33)",
            "rgba(202, 18, 25, .33)","rgba(168, 209, 226, .33)",

            "rgba(168, 207, 225, .37)","rgba(101, 72, 39, .37)","rgba(242, 129, 33, .37)",
            "rgba(0, 111, 177, .37)","rgba(234, 125, 174, .37)","rgba(213, 225, 198, .37)",
            "rgba(201, 19, 23, .37)","rgba(138, 88, 138, .37)","rgba(255, 213, 0, .37)",
            "rgba(171, 201, 135, .37)",
            "rgba(255, 213, 0, .37)","rgba(255, 128, 229, .37)","rgba(0, 111, 183, .37)",
        ]

        return (
            <View>
                <AnimatedHeader
                    scrollY={this.state.scrollY}
                    label={Languages.category}
                />
                <AnimatedScrollView
                    scrollEventThrottle={1}
                    contentContainerStyle={styles.scrollView}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        {useNativeDriver: true}
                    )}>
                    {typeof categories !== 'undefined' &&
                    categories.map((category, index) => {
                        if (category.parent == 0) {
                            // let imageCategory = Config.imageCategories[category.slug];
                            // if (imageCategory === undefined) {
                            //     imageCategory = Images.imageHolder;
                            // }
                            return (
                                <View style={styles.containerStyle} key={'catehome-' + index}>
                                    <TouchableScale
                                        style={styles.imageView}
                                        key={index + 'img'}
                                        onPress={() => this.showCategory(category)}>
                                        {/*<Image style={styles.image} source={imageCategory}/>*/}
                                        <Image
                                            source={Images.Logo}
                                            // source={require('@images/pattern1.jpeg')}
                                            style={styles.logo}
                                        />
                                        {/*<View style={[styles.overlay, { backgroundColor: backgroundColors[index]}]}>*/}
                                        <View style={styles.overlay}>
                                            <Text style={styles.title}>{category.name}</Text>
                                            {category.description.length > 0 && (
                                                <Text numberOfLines={2} style={styles.description}>
                                                    {category.description}
                                                </Text>
                                            )}
                                        </View>
                                    </TouchableScale>
                                </View>
                            );
                        }
                    })}
                </AnimatedScrollView>
            </View>
        );
    };

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                {this.renderContent()}

                {Config.showSwitchCategory && (
                    <FAB
                        buttonColor={Color.toolbarTint}
                        iconTextColor="#FFFFFF"
                        onClickAction={this.changeLayout}
                        visible
                        iconTextComponent={<Icon name="exchange"/>}
                    />
                )}
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const selectedCategory = state.categories.selectedCategory;
    const categories = flatten(state.categories.list);
    const selectedLayout = state.categories.selectedLayout;
    return {categories, selectedCategory, selectedLayout};
};
export default connect(
    mapStateToProps,
    {
        fetchCategories,
        setActiveLayout,
        setActiveCategory,
    }
)(CategoryHome);
