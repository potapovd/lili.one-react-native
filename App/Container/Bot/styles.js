/** @format */

import {StyleSheet, Dimensions} from 'react-native'
import {Color, Constants} from '@common'


export default StyleSheet.create({
    body: {
        flex: 1,
    },
    botchat: {
        flex: 1,
        paddingTop: 50,
    },
    typingText: {
        marginLeft: 10,
        marginBottom: 5,
        color: '#777',
    },
    leftWrapperStyle: {
        borderBottomWidth: 2,
        borderBottomColor: '#93c7ed',
        backgroundColor: '#b6e0ff',
        //backgroundColor: '#93c7ed',
        borderRadius: 20,
        borderTopLeftRadius: 5,
        padding: 5,
    },
    leftTextStyle: {
        color: '#333'
    },
    rightWrapperStyle: {
        //backgroundColor: UIConfigs.colorRightChat,
        //backgroundColor: '#b0d385',
        backgroundColor: '#fdd2bb',
        borderBottomWidth: 2,
        borderBottomColor: '#d9a88d',
        borderRadius: 20,
        borderBottomRightRadius: 5,
        marginBottom: 10,
        padding: 5,
        right: 15,
        justifyContent: 'flex-end',
        marginRight: 0,
    },
    rightTextStyle: {
        color: '#333'
    },

    sendContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
    sendInput: {
        backgroundColor: '#ffffff',
        borderColor: '#cccccc',
        padding: 0,
    },
    sendButton: {
        width: 20,
        height: 20,
    }

})
