import React, {Component} from 'react';
import {Image, Platform, Text, View} from 'react-native';
import {Bubble, GiftedChat, InputToolbar, Send} from 'react-native-gifted-chat';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import 'dayjs/locale/ru';
import {Dialogflow_V2} from 'react-native-dialogflow';
import {DialogflowConfig, Languages, Images} from '@common';

import {AnimatedHeader} from "@components";
import Animated from "react-native-reanimated";

import css from './styles'

const BOT = {
    _id: 2,
    name: 'Bot',
    avatar: Images.Robot,
    //sent: true,
};
const USER = {
    _id: 1,
    //name: 'Bot',
    //avatar: Images.Robot,
    //sent: true,
};
const quickRepliesArray = [
    {
        title: 'Медитации осознанности',
        value: 'Медитация осознанности',
    },
    {
        title: 'Медитации в движении - Цигун',
        value: 'Медитация в движении - Цигун',
    },
    {
        title: 'Статической медитации',
        value: 'Статическя медитация',
    },
]

const sayHi = [
    `Привет! Я робот Гуру-Медитации, поговорим об: `,
];

export default class Botchat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            scrollY: new Animated.Value(100),
        }
        Dialogflow_V2.setConfiguration(
            DialogflowConfig.client_email,
            DialogflowConfig.private_key,
            Dialogflow_V2.LANG_RUSSIAN,
            DialogflowConfig.project_id,
        )
    }

    onSend(messages = []) {
        if (messages[0].text.trim().length > 0) {
            this.setState(previousState => ({
                messages: GiftedChat.append(previousState.messages, messages),
                loading: true,
            }));

            let message = messages[0].text;
            Dialogflow_V2.requestQuery(
                message,
                result => this.handleGoogleResponse(result),
                error => console.log(error),
            );
        }
    }

    onQuickReply(quickReply) {
        let message = quickReply[0].value;
        let msg = [{
            _id: this.state.messages.length + 1,
            text: message,
            createdAt: new Date(),
            user: USER
        }]
        this.onSend(msg);
    }

    handleGoogleResponse(result) {
        let text = result.queryResult.fulfillmentMessages[0].text.text[0];
        this.sendBotResponse(text);
    }

    sendBotResponse(text) {
        let msg = {
            _id: this.state.messages.length + 1,
            text,
            createdAt: new Date(),
            user: BOT,
        };

        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, [msg]),
            loading: false,
        }));
        //console.log("H: "+Dimensions.get('GiftedChat').height)
    }

    componentDidMount() {
        if (Platform.OS == 'ios') {
            this.setState({defaultTabBarHeight: 49});
        }

        const item = sayHi[Math.floor(Math.random() * sayHi.length)];

        this.setState({
            messages: [
                {
                    _id: 1,
                    text: item,
                    createdAt: new Date(),
                    user: BOT,
                    quickReplies: {
                        type: 'radio', // or 'checkbox',
                        keepIt: true,
                        values: quickRepliesArray
                    },
                },
            ],
            //loading: false,
        });
    }


    _keyboardDidShow = (e) => {
        let keyboardHeight = e.endCoordinates.height;
        this.setState({
            minInputToolbarHeight: keyboardHeight + 45,
        });
    };

    _keyboardDidHide = () => {
        this.setState({
            minInputToolbarHeight: 45,
        });
    };


    render() {

        let bottomOffset = this.state.defaultTabBarHeight
        if (Platform.OS == 'ios') {
            bottomOffset = getBottomSpace() + this.state.defaultTabBarHeight
        }

        const customInputToolbar = props => {
            return (
                <InputToolbar
                    {...props}
                    containerStyle={css.sendInput}
                />
            );
        };

        const customSendButton = props => {
            return (
                <Send
                    {...props}
                    containerStyle={css.sendContainer}
                >
                    <Image
                        source={Images.SentButton}
                        style={css.sendButton}
                    />
                </Send>
            );
        };

        const customBubble = props => {
            return (
                <Bubble
                    {...props}
                    wrapperStyle={
                        {
                            left: css.leftWrapperStyle,
                            right: css.rightWrapperStyle
                        }
                    }
                    textStyle={{
                        left: css.leftTextStyle,
                        right: css.rightTextStyle,
                    }}
                />
            );
        };

        const customFooter = props => {

            if (this.state.loading) {
                return (
                    <Text
                        style={css.typingText}
                    >
                        Печатает....
                    </Text>);
            }
            return null;
        };

        const quickReplyStyle = {
            backgroundColor: '#fff'
        }

        return (
            // <SafeAreaView style={{flex: 1}}>
            <View style={{flex: 1}}>
                <AnimatedHeader
                    scrollY={this.state.scrollY}
                    label={Languages.bot}
                />
                {/*<ImageBackground*/}
                {/*    style={{*/}
                {/*        flex: 1,*/}
                {/*        width: '100%',*/}
                {/*        height: '100%',*/}
                {/*        backgroundColor: '#fff'*/}
                {/*    }}*/}
                {/*    //source={Images.ChatBackground}*/}
                {/*    resizeMode="repeat"*/}
                {/*    resizeMode='contain'*/}
                {/*>*/}
                <View style={{flex: 1}}>
                    <GiftedChat
                        messages={this.state.messages}
                        onSend={messages => this.onSend(messages)}
                        onQuickReply={quickReply => this.onQuickReply(quickReply)}
                        quickReplyStyle={quickReplyStyle}
                        placeholder='Сообщение...'
                        locale='ru'
                        alwaysShowSend={true}
                        renderInputToolbar={props => customInputToolbar(props)}
                        renderSend={props => customSendButton(props)}
                        renderBubble={props => customBubble(props)}
                        timeTextStyle={{left: {color: '#777'}, right: {color: '#777'}}}
                        listViewProps={{
                            keyboardDismissMode: 'on-drag',
                        }}
                        user={{
                            _id: 1,
                        }}
                        bottomOffset={bottomOffset}
                        renderAvatarOnTop={true}
                        renderFooter={props => customFooter(props)}
                    />


                </View>
                {/*</ImageBackground>*/}
            </View>
            // </SafeAreaView>
        );
    }


}
