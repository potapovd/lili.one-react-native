import React, {Component} from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {Color} from "@common";
import SplashScreen from 'react-native-splash-screen';

class NoConnection extends Component {
    componentDidMount() {
        SplashScreen.hide();
    }

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                padding: 20,
                backgroundColor: Color.containerBackground
            }}>
                <Icon
                    name='wifi-off'
                    size={70}
                    color={'#222222'}
                />
                <Text style={{
                    fontSize: 24,
                    textAlign: 'center',
                    marginBottom: 20,
                    color: '#222222',
                }}
                >
                    Нет интернет подключения
                </Text>
            </View>
        );
    }
};

export default NoConnection;
