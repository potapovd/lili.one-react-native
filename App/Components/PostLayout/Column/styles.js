/** @format */

import { Platform, StyleSheet, Dimensions, PixelRatio } from 'react-native'
import { Constants,Color } from '@common'

const { width, height } = Dimensions.get('window')

const widthPixelRatio = width * PixelRatio.get()

let imageHeight = 100;
// if( (width >= 1100) ){
//     imageHeight = 200;
// }else if (width >= 615) {
//     imageHeight = 300;
// } else {
//     imageHeight = 150;
// }

export default StyleSheet.create({
  panel: {
    position: 'relative',
    alignItems: 'center',
    marginBottom: 12,
  },
  image: {
    position: 'relative',
    //width: width,
    //width: (width) - 10,
    height: (width/3) + 20,
    borderRadius: 5,
  },
  name: {
    fontSize: 15,
    color: '#333',
    fontWeight: '400',
    //width: width,
    //width: (width/2) - 10,
    marginLeft: 8,
    marginRight: 8,
    marginTop: 8,
    fontFamily:
      Platform.OS !== 'android'
        ? Constants.fontHeader
        : Constants.fontHeaderAndroid,
    textAlign:'left',
  },
  time: {
    marginLeft: 8,
    marginRight: 8,
    color: '#999',
    fontSize: 10,
    marginTop: 4,
    fontFamily:
      Platform.OS !== 'android'
        ? Constants.fontFamily
        : Constants.fontHeaderAndroid,
    textAlign: 'left',
   alignSelf: 'flex-start'
  },
  heart: {
    position: 'absolute',
    zIndex: 9999,
    top: 1,
    right: 0,
  },

  iconPlay: {
    color: Color.playButtonArrow,
    backgroundColor: 'transparent',
    marginTop: 10,
    marginRight: 18,
    marginBottom: 10,
    marginLeft: 22,
    zIndex: 9999,
    width: 24,
  },
  iconVideo: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 50,
    left: width / 2 - 40,
    zIndex: 999,
    width: 50,
    height: 50,
    position: 'absolute',
    backgroundColor: Color.playButton,
    borderRadius: 40,
  },
})
