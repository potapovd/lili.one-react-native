/** @format */

import React, { Component } from 'react'
import { View } from 'react-native'
import Animated from "react-native-reanimated";

import Icons from '@navigation/Icons'
import styles from './styles'
import { Images, Constants } from '@common'
const headerMinHeight = 50

export default class AnimatedHeader extends Component {

  render() {
    const { scrollY, label, goBack, image, right } = this.props

    //console.log(scrollY, label, goBack, image, right);


    const titleTransformY = scrollY.interpolate({
      inputRange: [0, headerMinHeight],
      outputRange: [0, -20],
      extrapolate: 'clamp',
    })
    const titleTransformX = scrollY.interpolate({
      inputRange: [0, headerMinHeight],
      outputRange: [0, Constants.RTL ? -25 : 25],
      extrapolate: 'clamp',
    })
    const titleScale = scrollY.interpolate({
      inputRange: [0, headerMinHeight],
      outputRange: [1, 0.5],
      extrapolate: 'clamp',
    })

    const titleTextTransformX = scrollY.interpolate({
      inputRange: [0, headerMinHeight],
      outputRange: [0, Constants.RTL ? -25 : 25],
      extrapolate: 'clamp',
    })
    const titleTextTransformY = scrollY.interpolate({
      inputRange: [0, headerMinHeight],
      outputRange: [0, -30],
      extrapolate: 'clamp',
    })


    const navbarOpacity = scrollY.interpolate({
      inputRange: [0, headerMinHeight * 1.5],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    })

    return (
      <View style={styles.body}>
        <Animated.View
          style={[styles.headerView, { opacity: navbarOpacity }]}
        />

        {label && (
          <Animated.Text
            style={[
              styles.headerLabel,
              {
                transform: [
                  //{ translateY: titleTransformY },
                  { translateY: titleTextTransformY },
                  { translateX: titleTextTransformX },
                  //{ translateX: 5 },
                  //{ scale: titleScale },
                ],
              }
            ]}>
            {label}
          </Animated.Text>
        )}

        {image && (
          <Animated.Image
            source={image}
            style={[
              styles.headerImage,
              {
                transform: [
                  //
                  { translateY: titleTransformY },
                  //{ translateY: 20},
                  // { translateX: titleTransformX },
                  { translateX: 0 },
                  { scale: titleScale },
                ],
              },
            ]}
          />
        )}

        {typeof goBack != 'undefined' ? (
          <View style={[styles.homeMenu, ]}>
            {Icons.Back(goBack, Images.icons.shortBack)}
          </View>
        ) : (
          <View style={[styles.homeMenu, ]}>{Icons.Home()}</View>
        )}

        {right && (
          <Animated.View
            style={[
              styles.headerRight,
              { transform: [{ translateY: titleTransformY }] }
            ]}>
            {right}
          </Animated.View>
        )}
      </View>
    )
  }
}
