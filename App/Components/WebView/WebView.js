/** @format */

import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {Dimensions, Platform, Text} from 'react-native'

const {width, scale} = Dimensions.get('window')
const newScale = Platform.OS == 'ios' ? scale : 1
import {WebView} from "react-native-webview";

export default class WebViewComponent extends PureComponent {
    static propTypes = {
        html: PropTypes.any,
    }

    constructor(props) {
        super(props)

        this.state = {
            webViewHeight: 600,
        }
    }

    updateHeight = (event) => {
        this.setState({webViewHeight: parseInt(event.jsEvaluationValue)})
    }

    render() {
        const getHTML = () => {
            const html = this.props.html

            return `<html><head><style type="text/css">
				      body {
				        font: 14pt arial, sans-serif;
				        background: white;
				      }
				      a, h1, h2, h3, p, span, font, li, div {
				        font: 24px arial, sans-serif !important;
				      }
				      body .about-me-out{
				        text-align: center !important;
				        margin: 0px auto !important;
				        padding: 40px !important;
				      }
				      body .about-me-out img{
				        width: 400px !important;
				        height: auto !important;
				        margin: 0px auto 20px !important;
				      }
				      body .about-me-txt{
				        font-size: 22px !important;
				        padding: 40px 20px !important;
				        text-align: center !important;
				      }
				      img {
				        height: auto;
				        width: ${(width - 15) * newScale}px
              }
				</style></head><body>${html}</body>`
        }

        return (
            <WebView
                source={{html: getHTML()}}
                injectedJavaScript="document.body.scrollHeight;"
                onNavigationStateChange={this.updateHeight}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                scrollEnabled={false}
                scalesPageToFit={true}
                sharedCookiesEnabled={true}
                startInLoadingState={true}
                style={{
                    height: 900,
                    fontSize: 30,
                }}
            />
        )
    }
}
