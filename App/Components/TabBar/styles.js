/** @format */

import {Platform, StyleSheet} from 'react-native'
import {Color} from '@common'

export default StyleSheet.create({
    tabbar: {
        height: 49,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.tabbar,
        borderTopWidth: 1/2,
        borderTopColor: Color.tabbarTopBorder
    },
    tab: {
        alignSelf: 'stretch',
        flex: 1,
        alignItems: 'center',
        ...Platform.select({
            ios: {
                justifyContent: 'center',
                paddingTop: 0,
            },
            android: {
                justifyContent: 'center',
            },
        }),
    },
})
